import "@/style/index.scss"
import {Game} from "./js/game.js"
import sudokuJson from "../test/json/difficult/sudoku_uncomplete";

var game = new Game();
window.game=game;
game.loadSudoku(sudokuJson)
