//use as an exemple of the model;
const sudokuModel= [];

const sudokuCaseModel= {
  potentialSolution :[1,2,3,4,5,6,7,8,9],
  index:-1
};

const possibilities=[1,2,3,4,5,6,7,8,9];

const SudokuCase=function(potentialSolution=possibilities,index){
  if (typeof potentialSolution === "number" && potentialSolution!=0) {
    potentialSolution=[potentialSolution];
  }else if(typeof potentialSolution !== "object"){
    potentialSolution=possibilities;
  }
  return {
    "potentialSolution" :potentialSolution,
    "index":index
  };
};

export{SudokuCase};
