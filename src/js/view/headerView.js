import {utilAddEventListener,utilRemoveAllEventListener} from "./../utils/EventsListenerUtil.js"
import {showPopupImportSudoku,hidePopupImportSudoku} from "./importSudokuView.js"


let popupEventsQueue = [];

const listenButtonHeaderImport= function(){
  var importButton=document.getElementsByClassName('import-button')[0];
  popupEventsQueue.push(utilAddEventListener(importButton,'click', function () {
    showPopupImportSudoku();
  }));
};

export {listenButtonHeaderImport}
