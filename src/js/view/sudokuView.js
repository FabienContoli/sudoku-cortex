import {utilAddEventListener,utilRemoveAllEventListener} from "./../utils/EventsListenerUtil.js"
import {listenButtonHeaderImport} from "./headerView.js"

let popupEventsQueue = [];
const initSudokuView=()=>{
    popupEventsQueue=[];
    listenButtonHeaderImport();
    initClickCase();
};

const getDomElementSudokuCase= (nbCase)=>{
  var sudokuCase=document.getElementsByClassName("sudoku-square-case-text")[nbCase];
  return sudokuCase;
}

const changeCaseFocus=(indexToAdd)=>{
  var sudokuActiveCase=document.getElementsByClassName('sudoku-square-is-active');
  var actualIndex=0;
  if(sudokuActiveCase.length!=0){
    actualIndex=parseInt(sudokuActiveCase[0].getElementsByClassName("sudoku-square-case-text")[0].dataset.number);
    console.log("actualIndex:"+actualIndex);

  }
  var nextIndex=actualIndex+indexToAdd;
  console.log("nextIndex:"+nextIndex);

  if(nextIndex>=0 &&nextIndex<=80){
    console.log("showPopup:"+nextIndex);

    document.getElementsByClassName('sudoku-square')[nextIndex].classList.add("sudoku-square-is-active");
    showPopup(nextIndex);

  }
}

const changeSolution=(solution)=>{
  var sudokuCasesActive=document.getElementsByClassName('sudoku-square-is-active');
  if(sudokuCasesActive.length==1){
    var sudokuCaseActive = sudokuCasesActive[0];
    var sudokuCaseText=sudokuCaseActive.getElementsByClassName("sudoku-square-case-text")[0];
    sudokuCaseText.textContent = solution;
    showPopup(sudokuCaseText.dataset.number);
    sudokuCaseActive.classList.add("sudoku-square-is-active");
    changeCaseFocus(1);
  }

}

const initClickCase = () =>{
  var tabCase = document.getElementsByClassName('sudoku-square-case-text');

  for (var i = 0; i < tabCase.length; i++) {
    tabCase[i].dataset.number = i;
    tabCase[i].addEventListener('click', function(e) {
      console.log(e.target.dataset.number);
      document.getElementsByClassName('sudoku-square')[e.target.dataset.number].classList.add("sudoku-square-is-active");
      showPopup(e.target.dataset.number);
    }, false);

  }
};

const removeClassAndPopupandEvents=(popup)=>{
  popupEventsQueue=utilRemoveAllEventListener(popupEventsQueue);
  clearPopup();
  if (popup.dataset.number) {
    document.getElementsByClassName("sudoku-square")[popup.dataset.number].classList.remove("sudoku-square-is-active");
  }
}

const showPopup = (nb_case)=> {
  var popup = document.getElementsByClassName('popup')[0];
  var sudokuCase = document.getElementsByClassName("sudoku-square-case-text")[nb_case];

  removeClassAndPopupandEvents(popup);

  popup.dataset.number = nb_case;
  var multipleChoice = (sudokuCase.dataset.multichoice == "true");
  var tabChoices = [];

  if (sudokuCase.textContent != "") {
    var listChoice = sudokuCase.textContent.split(" ");
    for (var i = 0; i < listChoice.length; i++) {
      tabChoices.push(listChoice[i]-1);
      document.getElementsByClassName('popup-number')[listChoice[i]-1].classList.add('popup-number-is-active');
    }
   }
  var firstClick = true;
    if (!popup.classList.contains("popup-is-visible")) {
      popup.classList.add("popup-is-visible");
    }

  for (var i = 0; i < 9; i++) {
    popupEventsQueue.push(utilAddEventListener(document.getElementsByClassName('popup-number')[i], 'click', function(e) {
      console.log(e.target.dataset.number);

        if (tabChoices.length == 1) {
          document.getElementsByClassName('popup-number')[tabChoices[0]].classList.remove('popup-number-is-active');
        }

        if (tabChoices.length == 1 && tabChoices[0] == e.currentTarget.dataset.number-1) {
          tabChoices = [];
          sudokuCase.textContent = "";
        } else {
          tabChoices[0] = e.currentTarget.dataset.number-1;
          e.currentTarget.classList.add('popup-number-is-active');
          sudokuCase.textContent = e.currentTarget.dataset.number;
        }
      }));
  }

  popupEventsQueue.push(utilAddEventListener(document.getElementsByClassName('popup-btn-clear')[0], 'click', function(e) {
    clearPopup();
    tabChoices = [];
    document.getElementsByClassName("sudoku-square-case-text")[document.getElementsByClassName('popup')[0].dataset.number].textContent = "";
  }));

  popupEventsQueue.push(utilAddEventListener(document.getElementsByClassName('popup-btn-quote')[0], 'click', function(e) {
    var sudokuCase = document.getElementsByClassName("sudoku-square-case-text")[document.getElementsByClassName('popup')[0].dataset.number];
    if (sudokuCase.dataset.multichoice == false) {
      sudokuCase.dataset.multichoice = true;
    } else {
      sudokuCase.dataset.multichoice = false;
      if (tabChoices.length > 0) {
        var tabChoicesTemp = [];
        tabChoicesTemp.push(tabChoices[0]);
        tabChoices = tabChoicesTemp;
      }
    }
  }));
};

const clearPopup = ()=> {
  for (var i = 0; i < 9; i++) {
    if (document.getElementsByClassName('popup-number')[i].classList.contains('popup-number-is-active')) {
      document.getElementsByClassName('popup-number')[i].classList.remove('popup-number-is-active');
    }
  }
};

export {initSudokuView,getDomElementSudokuCase,changeCaseFocus,changeSolution};
