
import {utilAddEventListener,utilRemoveAllEventListener} from "./../utils/EventsListenerUtil.js"

let popupEventsQueue = [];

const listenButtonImportSudoku= function(){
  var inputFile=document.getElementsByClassName('import-sudoku-file')[0];
  popupEventsQueue.push(utilAddEventListener(inputFile,'change', function () {
    var reader = new FileReader();
    reader.readAsDataURL(inputFile.files[0]);
    console.log('Done');
  }));
};


const listenButtonCancelSudoku= function(){
  var button=document.getElementsByClassName('import-sudoku-button-cancel')[0];
  popupEventsQueue.push(utilAddEventListener(button,'click', function () {
    hidePopupImportSudoku();
  }));
};

const listenButtonValidateSudoku= function(){
  var button=document.getElementsByClassName('import-sudoku-button-validate')[0];
  popupEventsQueue.push(utilAddEventListener(button,'click', function () {
    hidePopupImportSudoku();
  }));
};

const showPopupImportSudoku =function(){
    listenButtonImportSudoku();
    listenButtonCancelSudoku();
    listenButtonValidateSudoku();
    document.getElementsByClassName('popin-import-sudoku-container')[0].classList.remove("invisible");
}

const hidePopupImportSudoku=function(){
  popupEventsQueue=utilRemoveAllEventListener(popupEventsQueue);
  document.getElementsByClassName('popin-import-sudoku-container')[0].classList.add("invisible");
}



export{showPopupImportSudoku,hidePopupImportSudoku}
