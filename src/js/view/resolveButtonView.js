import {saveSudoku,loadSudoku} from "./../utils/debug.js";
import {resolveSudoku} from "./../logic/resolve.js";

const ResolveButtonView = function(){

     initClickBtnHint();

     initClickBtnResolveNext();

     initClickBtnResolve();
}

const initClickBtnHint =function() {
  var btn = document.getElementsByClassName('btn-hint');

  btn[0].addEventListener('click',
    function(e) {
      console.log(e.target)
    }, false);

};

const initClickBtnResolveNext= function() {
  var btn = document.getElementsByClassName('btn-resolve-next');

  btn[0].addEventListener('click',
    function(e) {
      console.log(e.target)
    }, false);

};

const initClickBtnResolve=function () {
  var btn = document.getElementsByClassName('btn-resolve-all');

  btn[0].addEventListener('click',
    function(e) {
      try{
        var resolved=resolveSudoku(saveSudoku());
        loadSudoku(resolved);
        console.log(e.target);
      }catch(err){
        alert("no solution found")
      }
    }, false);

};

export {ResolveButtonView};
