import {ResolveButtonView} from "./view/resolveButtonView.js";
import {initSudokuView} from "./view/sudokuView.js";
import {saveSudoku,loadSudoku} from "./utils/debug.js";
import {resolveSudoku} from "./logic/resolve.js";
import {activateKeyboardNavigation} from "./utils/keyboardUtils.js";;

const Game = function(){
    new initSudokuView();
    new ResolveButtonView();

    this.save=function(){
      var saved=saveSudoku();
      console.log(JSON.stringify(saved));
      return saved;
    }
    this.loadSudoku=loadSudoku;
    this.resolveSudoku=function(){
      var startDate=new Date();
      var resolved=resolveSudoku(saveSudoku());
      console.log(resolved);
      var endDate=new Date();
      console.log("solution Found in:"+(endDate-startDate)+" ms");
      loadSudoku(resolved);
      return resolved;
    }

    this.keyboardEvent=activateKeyboardNavigation();
}

export {Game};
