import {utilAddEventListener} from "./../utils/EventsListenerUtil.js"
import {changeCaseFocus,changeSolution} from "./../view/sudokuView.js"
const activateKeyboardNavigation  = () => {
  return utilAddEventListener(document, 'keydown', navigationLogicEvent);
}

const navigationLogicEvent=(e)=>{
  //wich is more widely supported than keycode
  switch(e.which) {

    case 8: // backspace
        changeSolution();
    break;

    case 32: // space bar
        changeSolution();
        move(1);
    break;

    case 37 || (e.shiftKey && 9): // left
        move(-1);
    break;

    case 38: // up
       move(-9); //Dont know if you want to use it
    break;

    case (39 || 9): // right
       move(1);
    break;

    case 40: // down
       move(9);
    break;

    case ((e.which>=97 && e.which<=105)?e.which:-1): //case 1 to 9
        changeSolution(e.which-96);
    break;

     default: return;
 }
 e.preventDefault();
}

const move=(indexToAdd)=>{
  changeCaseFocus(indexToAdd);
}

export {activateKeyboardNavigation}
