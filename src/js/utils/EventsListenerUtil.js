const utilAddEventListener = (element, eventType='click', handler)=> {
  element.addEventListener(eventType, handler);
  return {
    element,
    eventType,
    handler
  };
};

const utilRemoveEventListener = function(eventRegistered) {
  eventRegistered.element.removeEventListener(eventRegistered.eventType, eventRegistered.handler)
};

const utilRemoveAllEventListener = function(eventRegisteredList) {
  for (var i = 0; i < eventRegisteredList.length; i++) {
    utilRemoveEventListener(eventRegisteredList[i]);
  }
  return [];
};

export {utilAddEventListener,utilRemoveEventListener,utilRemoveAllEventListener}
