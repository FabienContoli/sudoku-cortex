import {SudokuCase} from "./../model/sudokuModel.js"
import {getDomElementSudokuCase} from './../view/sudokuView.js';

const saveSudoku = () => {
  var tabCase = document.getElementsByClassName('sudoku-square-case-text');

  var sudokuCaseMap=[];
  for (var i = 0; i < tabCase.length; i++) {
      sudokuCaseMap.push(new SudokuCase(parseInt(tabCase[i].textContent)||0,i));
  }
  return sudokuCaseMap;
};

const loadSudoku =(jsonSudoku)=>{
  var sudokuCaseMap=jsonSudoku;
  for (var i = 0; i < jsonSudoku.length; i++) {
      if(jsonSudoku[i].potentialSolution.length ==1){
        getDomElementSudokuCase(i).textContent=jsonSudoku[i].potentialSolution[0];
      }else{
        getDomElementSudokuCase(i).textContent="";
      }
  }
  return sudokuCaseMap;
}

export {saveSudoku,loadSudoku}
