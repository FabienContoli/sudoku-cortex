import {findRowSudoku,findColumnSudoku,findSquareSudoku,findAlignPairOrTripleInSquare} from "./find.js";
import {checkIntegritySudokuUnfinish} from "./integrity.js";
import {excludePotentialSolutionFromCollection,excludeInexistantPotentialSolutionFromCollection,excludeAlignPairOrTripleFromSquareOfCollection} from "./exclude.js";
import {getIndex,getPotentialSolution} from "./model.js";
import {bruteForce} from "./bruteForce.js";

const resolveSudoku=(mapSudokuCase)=>{
  var nbSolution=0;
  for(var i=0;i<mapSudokuCase.length;i++){
    if(getPotentialSolution(mapSudokuCase[i]).length===1){
      nbSolution++;
    }
  }

  var arrayFindFunctions=[findRowSudoku,findColumnSudoku,findSquareSudoku];
  var timeout=0;
  while(nbSolution!==81 && timeout<3){

    if(!checkIntegritySudokuUnfinish(mapSudokuCase)){
      console.log("error Critical");
      throw new Error("failed");
    };
    console.log("nbSolution="+nbSolution);
    var nbSolutionAtLoopStart=nbSolution;
    for(var i=0;i<mapSudokuCase.length;i++){
      if(getPotentialSolution(mapSudokuCase[i]).length!==1){
        for(var iFunct=0;iFunct<arrayFindFunctions.length;iFunct++){
            var casesGroup=arrayFindFunctions[iFunct](mapSudokuCase)(mapSudokuCase[i]);
            mapSudokuCase[i]=excludePotentialSolutionFromCollection(casesGroup)(mapSudokuCase[i]);
            if(getPotentialSolution(mapSudokuCase[i]).length!==1){
              mapSudokuCase[i]=excludeInexistantPotentialSolutionFromCollection(casesGroup)(mapSudokuCase[i]);
            }
            if(getPotentialSolution(mapSudokuCase[i]).length===1){
              nbSolution++;
              break;
            }
        }
      }
    }
    //après avoir retirer les solutions évidentes ( exclusion et inclusion)
    //-> on cherche les pair et les triples
    if(nbSolutionAtLoopStart ==nbSolution){
      for(var squareNb=0;squareNb<9;squareNb++){
        var square=findSquareSudoku(mapSudokuCase)(mapSudokuCase[(Math.floor(squareNb/3)*27)+(squareNb%3)*3]);
        var alignPairOrTripleArray=findAlignPairOrTripleInSquare(square);
        for(var index=0;index<alignPairOrTripleArray.length;index++){
          var line=[];
          if(alignPairOrTripleArray[index].direction=='row')line=findRowSudoku(mapSudokuCase)(alignPairOrTripleArray[index].cases[0]);
          else line=findColumnSudoku(mapSudokuCase)(alignPairOrTripleArray[index].cases[0]);
          nbSolution=excludeAlignPairOrTripleFromSquareOfCollection(line)(alignPairOrTripleArray[index])(nbSolution);
          if(!checkIntegritySudokuUnfinish(mapSudokuCase)){
            console.log("error Critical");
            throw new Error("failed");
          };
        }
      }
    }
    // si on a pas trouvé de solution on fait du brute force
    if(nbSolutionAtLoopStart==nbSolution && timeout==2){
      console.log("brute force attempt");
      try{
        mapSudokuCase=bruteForce(mapSudokuCase);
      }catch(err){
        console.log("error Critical:"+err);
        throw new Error("failed");
      }
    }

    if(nbSolutionAtLoopStart==nbSolution){
      timeout++;
    }else {
      timeout=0;
    }
  }
  if(!checkIntegritySudokuUnfinish(mapSudokuCase)){
    console.log("error Critical");
    throw new Error("failed");
  };
  return mapSudokuCase;
};


export{resolveSudoku};
