import {getPotentialSolution,getIndex} from './model';
import {resolveSudoku} from "./resolve";
import {checkIntegritySudoku} from "./integrity";

const bruteForce= mapSudokuCase =>{
    var mapSudokuCaseTemp=JSON.parse(JSON.stringify(mapSudokuCase));
    var arrayCasesToTest=[];
    for(var i=0;i<81;i++){
        if(getPotentialSolution(mapSudokuCaseTemp[i]).length>1){
          arrayCasesToTest.push(mapSudokuCaseTemp[i]);
        }
    }

    arrayCasesToTest.sort((case1,case2)=>{
      if (getPotentialSolution(case1).length > getPotentialSolution(case2).length) {
        return 1;
      }
      if (getPotentialSolution(case1).length < getPotentialSolution(case2).length) {
        return -1;
      }
      return 0;
    });

    var solutionNotFind=true;
    while(solutionNotFind || arrayCasesToTest.length>0){
      var caseToTest=arrayCasesToTest.shift();
      var caseToTestTemp=JSON.parse(JSON.stringify(caseToTest));
      while(solutionNotFind ||getPotentialSolution(caseToTestTemp).length>0){
        try{
          var potentialSolution=getPotentialSolution(caseToTestTemp);
          var solutionToTest=potentialSolution.pop();
          var indexToTest=getIndex(caseToTestTemp);
          var mapSudokuCaseToTest=JSON.parse(JSON.stringify(mapSudokuCaseTemp));
          mapSudokuCaseToTest[indexToTest].potentialSolution=[solutionToTest];
          try{
            mapSudokuCaseToTest=resolveSudoku(mapSudokuCaseToTest);
            if(checkIntegritySudoku(mapSudokuCaseToTest)){
              solutionNotFind=false;
              mapSudokuCase=mapSudokuCaseToTest;
            }else{
              throw new Error("no solution found");
            }
          }catch(err){
            throw err;
          }
        }catch(err){
          if(getPotentialSolution(mapSudokuCaseTemp[indexToTest]).length>1){

            getPotentialSolution(mapSudokuCaseTemp[indexToTest]).splice(getPotentialSolution(mapSudokuCaseTemp[indexToTest]).indexOf(solutionToTest),1);
          }else{
            throw err;
          }
        }
      }
    }

  return mapSudokuCase;
};

export {bruteForce};
