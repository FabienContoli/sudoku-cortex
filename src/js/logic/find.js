import {getIndex,getPotentialSolution} from './model';

const isInSameRow = index=>indexToCompare=>{
  if(Math.floor(indexToCompare/9)===Math.floor(index/9)){
    return true;
  }
  else return false;
}

const isInSameColumn = index=>indexToCompare=>{
  if(indexToCompare%9===index%9){
    return true;
  }
  else return false;
}
const isInSameSquare = index=>indexToCompare=>{
  if( Math.floor(indexToCompare/3)%3 === Math.floor(index/3)%3 && Math.floor(Math.floor(indexToCompare/9)/3) ==Math.floor(Math.floor(index/9)/3)){
    return true;
  }
  else return false;
}

const filterElementInCollectionByCompareElement=(predicat)=>(mapper)=>(collection)=>(elementToTest)=>{
  return collection.filter((element) => {
    return predicat(mapper(element))(mapper(elementToTest));
  });
};

const findRowSudoku =(mapSudokuCase)=>(sudokuCase)=>{
  return filterElementInCollectionByCompareElement(isInSameRow)(getIndex)(mapSudokuCase)(sudokuCase);
}

const findColumnSudoku =(mapSudokuCase)=>(sudokuCase)=>{
  return filterElementInCollectionByCompareElement(isInSameColumn)(getIndex)(mapSudokuCase)(sudokuCase);
}

const findSquareSudoku =(mapSudokuCase)=>(sudokuCase)=>{
  return filterElementInCollectionByCompareElement(isInSameSquare)(getIndex)(mapSudokuCase)(sudokuCase);
}

const findAlignPairOrTripleInSquare=(collection)=>{
  //model:  alignPairOrTripleArray=[alignPairOrTriple:{solutionToExclude:number,cases=[SudokuCases],alignFunction:function}];
  var alignPairOrTripleArray=[];
  for(var i=1;i<10;i++){
    var alignPairOrTriple={};
    var cases=[];
    for (var index=0;index<collection.length;index++){
      if(getPotentialSolution(collection[index]).indexOf(i)!==-1 && getPotentialSolution(collection[index]).length==1){
        cases=[];
        break;
      }else if(getPotentialSolution(collection[index]).indexOf(i)!==-1){
        cases.push(collection[index]);
      }
    }
    if( cases.length>1 && cases.length<4 ){
      var isAlign=true;
      var funcIsAlign=[isInSameColumn,isInSameRow];
      for(var funcIndex=0;funcIndex<funcIsAlign.length;funcIndex++){
          for(var indexCases=0;indexCases<cases.length-1;indexCases++){
            isAlign=(isAlign&&funcIsAlign[funcIndex](cases[indexCases].index)(cases[indexCases+1].index));
          }
          if(isAlign===true){
            var direction=["column","row"];
            alignPairOrTriple.direction=direction[funcIndex];
            break;
          }
      }
      if(isAlign){
        alignPairOrTriple.solutionToExclude=i;
        alignPairOrTriple.cases=cases;
        alignPairOrTripleArray.push(alignPairOrTriple);
      }
    }
  }

  return alignPairOrTripleArray;
}


export {isInSameRow,isInSameColumn,isInSameSquare,findRowSudoku,findColumnSudoku,findSquareSudoku,findAlignPairOrTripleInSquare};
