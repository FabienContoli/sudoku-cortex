const getIndex=(element)=>{
  return element.index;
}

const getPotentialSolution=(element)=>{
  return element.potentialSolution;
}

export{getIndex,getPotentialSolution}
