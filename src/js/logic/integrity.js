import {getPotentialSolution} from './model';
import {findRowSudoku,findColumnSudoku,findSquareSudoku} from "./find.js";

const checkIntegritySudoku = mapSudokuCase =>{

  var arrayFindFunctions=[findRowSudoku,findColumnSudoku,findSquareSudoku];
  var isValid=true;
  if(mapSudokuCase.length !== 81) return false;

  for(var i=0;i<mapSudokuCase.length;i++){
    for(var iFunct=0;iFunct<arrayFindFunctions.length;iFunct++){
        var casesGroup=arrayFindFunctions[iFunct](mapSudokuCase)(mapSudokuCase[i]);
        if(!checkIntegrityFromCollection(casesGroup)){
          isValid= false;
          break;
        };
      }
      if(!isValid)break;
    }

  return isValid;
}

const checkIntegritySudokuUnfinish = mapSudokuCase =>{
  var arrayFindFunctions=[findRowSudoku,findColumnSudoku,findSquareSudoku];
  var isValid=true;

  for(var i=0;i<mapSudokuCase.length;i++){
    for(var iFunct=0;iFunct<arrayFindFunctions.length;iFunct++){
        var casesGroup=arrayFindFunctions[iFunct](mapSudokuCase)(mapSudokuCase[i]);
        if(!checkPartialIntegrityFromCollection(casesGroup)){
          isValid= false;
          break;
        };
      }
      if(!isValid)break;
    }
  return isValid;
}

const checkPartialIntegrityFromCollection= collection =>{
  if(collection.length !== 9) return false;
  var arrayNumberSudoku=[];
  var isValid=true;
  for(var index=0;index<collection.length;index++){
    if(getPotentialSolution(collection[index]).length<2){
      if((getPotentialSolution(collection[index]).length===1 && getPotentialSolution(collection[index])[0]>0 &&
       getPotentialSolution(collection[index])[0]<10 && arrayNumberSudoku.indexOf(getPotentialSolution(collection[index])[0])==-1)){
        arrayNumberSudoku.push(getPotentialSolution(collection[index])[0]);
      }else {
        console.log("error Integrity :"+collection[index]);
        isValid= false;
        break;
      }
    }
  }
  return isValid;
}

const checkIntegrityFromCollection= collection =>{
  if(collection.length !== 9) return false;
  var arrayNumberSudoku=[];
  var isValid=true;
  for(var index=0;index<collection.length;index++){
    if(getPotentialSolution(collection[index]).length===1 && getPotentialSolution(collection[index])[0]>0 &&
     getPotentialSolution(collection[index])[0]<10 && arrayNumberSudoku.indexOf(getPotentialSolution(collection[index])[0])==-1){
      arrayNumberSudoku.push(getPotentialSolution(collection[index])[0]);
    }else {
      isValid= false;
      break;
    }
  }

  return isValid;
}

export{checkIntegritySudoku,checkIntegrityFromCollection,checkIntegritySudokuUnfinish,checkPartialIntegrityFromCollection}
