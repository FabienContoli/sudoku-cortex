const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: './src/index.js',
  //entry: './src/js/sudoku.js',
  devtool: 'inline-source-map',
  devServer: {
     contentBase: './dist'
 },
  output: {
    filename: '[name].[hash:8].js',
    sourceMapFilename: '[name].[hash:8].map',
    chunkFilename: '[id].[hash:8].js',
    path: path.resolve(__dirname, 'dist')
  },
  resolve: {
    alias: {
      "@": path.resolve(__dirname, 'src/'),
      "assets": path.resolve(__dirname, 'src/assets'),
      "test": path.resolve(__dirname, 'test/')
    }
  },
  module: {
      rules: [
        {
          test: /\.css$/,
          use: [
            'style-loader',
            'css-loader'
          ]
        },
        {
          test: /\.scss$/,
          use: [
            // fallback to style-loader in development
            process.env.NODE_ENV !== 'production' ? 'style-loader' : MiniCssExtractPlugin.loader,
            'css-loader',
            'postcss-loader',
            'sass-loader',
          ],
        },
         {
           test: /\.(png|svg|jpg|gif)$/,
           use: [
             'file-loader'
           ]
         },
         {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader'
          }
        }
      ]
    },
    plugins: [
      new CleanWebpackPlugin(['dist']),
      new HtmlWebpackPlugin({
       template : './src/index.template.ejs'
     }),
    new MiniCssExtractPlugin({
      filename: '[name].css',
    }),
   ],
   mode: "development"
};
