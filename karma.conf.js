var webpackConfig = require('./webpack.config.js');

module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    files: [
      { pattern: 'test/*_test.js', watched: true },
      { pattern: 'test/**/*_test.js', watched: true }
    ],
    preprocessors: {
      'test/*_test.js': [ 'webpack','sourcemap' ],
      'test/**/*_test.js': [ 'webpack','sourcemap' ]
    },
    webpack: webpackConfig,
    reporters: ['mocha'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['PhantomJS'],
    singleRun: false,
    concurrency: Infinity
  });
};
