const serve = require('serve');

const server = serve(__dirname+"/src", {
  port: 80,
  ignore: ['node_modules']
})
