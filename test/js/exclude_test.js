import {excludePotentialSolutionFromCollection,excludeSolution,excludeInexistantPotentialSolution,excludeAlignPairOrTripleFromSquareOfCollection,excludeInexistantPotentialSolutionFromCollection} from '../../src/js/logic/exclude';
import {SudokuCase} from '../../src/js/model/sudokuModel';
import {getPotentialSolution} from '../../src/js/logic/model';

/*

 grille sudoku

 0 1  2  3  4  5  6  7  8
 9 10 11 12 13 14 15 16 17
18 19 20 21 22 23 24 25 26
27 28 29 30 31 32 33 34 35
36 37 38 39 40 41 42 43 44
45 46 47 48 49 50 51 52 53
54 55 56 57 58 59 60 61 62
63 64 65 66 67 68 69 70 71
72 73 74 75 76 77 78 79 80

*/


describe('exclude', function() {
  describe('excludeSolution()', function() {
    it('should return potentialSolution [4,7]', function() {

      var testedCase=new SudokuCase([2,4,7],0);
      var testCase = new SudokuCase([2],1);

      testedCase=excludeSolution(testedCase)(testCase);
      expect(getPotentialSolution(testedCase).length).toBe(2);
      expect(getPotentialSolution(testedCase)).toContain(4);
      expect(getPotentialSolution(testedCase)).toContain(7);

    });

    it('should return potentialSolution [2,4,7]', function() {

      var testedCase=new SudokuCase([2,4,7],0);
      var testCase = new SudokuCase([2,4],1);

      testedCase=excludeSolution(testedCase)(testCase);
      expect(getPotentialSolution(testedCase).length).toBe(3);
      expect(getPotentialSolution(testedCase)).toContain(2);
      expect(getPotentialSolution(testedCase)).toContain(4);
      expect(getPotentialSolution(testedCase)).toContain(7);

    });
  });

  describe('excludePotentialSolutionFromCollection()', function() {
    it('should return potentialSolution [7]', function() {

      var testedCase=new SudokuCase([2,4,7],0);
      var testCase=new SudokuCase([4],1);
      var testCase2=new SudokuCase([2],2);
      var collection=[];
      collection.push(testCase);
      collection.push(testCase2);
      testedCase=excludePotentialSolutionFromCollection(collection)(testedCase);

      expect(getPotentialSolution(testedCase).length).toBe(1);
      expect(getPotentialSolution(testedCase)).toContain(7);
    });
  });

  describe('excludeInexistantPotentialSolution()', function() {
    it('should return potentialSolution [2]', function() {
      var potentialSolutionInexistant=[1,2,3,5,7,8];
      var element=new SudokuCase([1,3,5,7,8],1);
      potentialSolutionInexistant=excludeInexistantPotentialSolution(potentialSolutionInexistant)(element);
      expect(potentialSolutionInexistant.length).toBe(1);
      expect(potentialSolutionInexistant).toContain(2);
    });
  });


  describe('excludeInexistantPotentialSolution()', function() {
    it('should return potentialSolution [1,2,3,5,7,8]', function() {
      var potentialSolutionInexistant=[1,2,3,5,7,8];
      var element=new SudokuCase([9],1);
      potentialSolutionInexistant=excludeInexistantPotentialSolution(potentialSolutionInexistant)(element);
      expect(potentialSolutionInexistant.length).toBe(6);
      expect(potentialSolutionInexistant).toContain(1);
      expect(potentialSolutionInexistant).toContain(2);
      expect(potentialSolutionInexistant).toContain(3);
      expect(potentialSolutionInexistant).toContain(5);
      expect(potentialSolutionInexistant).toContain(7);
      expect(potentialSolutionInexistant).toContain(8);
    });
  });

  describe('excludeInexistantPotentialSolutionFromCollection()', function() {
    it('should return potentialSolution [2]', function() {

      var testedCase=new SudokuCase([1,2,3,5,7,8],0);
      var testCase=new SudokuCase([4],1);
      var testCase2=new SudokuCase([9],2);
      var testCase3=new SudokuCase([1,3,5,7,8],3);
      var testCase4=new SudokuCase([1,3,5,7,8],4);
      var testCase5=new SudokuCase([6],5);
      var testCase6=new SudokuCase([5,7,8],6);
      var testCase7=new SudokuCase([1,7,8],7);
      var testCase8=new SudokuCase([1,3,5,7,8],8);
      var collection=[];
      collection.push(testCase);
      collection.push(testCase2);
      collection.push(testCase3);
      collection.push(testCase4);
      collection.push(testCase5);
      collection.push(testCase6);
      collection.push(testCase7);
      collection.push(testCase8);
      testedCase=excludeInexistantPotentialSolutionFromCollection(collection)(testedCase);
      expect(getPotentialSolution(testedCase).length).toBe(1);
      expect(getPotentialSolution(testedCase)).toContain(2);
    });
  });

  describe('excludeInexistantPotentialSolutionFromCollection()', function() {
    it('should return potentialSolution [1,2,3,5,7,8]', function() {

      var testedCase=new SudokuCase([1,2,3,5,7,8],0);
      var testCase=new SudokuCase([4],1);
      var testCase2=new SudokuCase([9],2);
      var testCase3=new SudokuCase([1,3,5,7,8],3);
      var testCase4=new SudokuCase([1,3,5,7,8],4);
      var testCase5=new SudokuCase([6],5);
      var testCase6=new SudokuCase([5,7,8],6);
      var testCase7=new SudokuCase([1,2,7,8],7);
      var testCase8=new SudokuCase([1,2,3,5,7,8],8);
      var collection=[];
      collection.push(testCase);
      collection.push(testCase2);
      collection.push(testCase3);
      collection.push(testCase4);
      collection.push(testCase5);
      collection.push(testCase6);
      collection.push(testCase7);
      collection.push(testCase8);
      testedCase=excludeInexistantPotentialSolutionFromCollection(collection)(testedCase);
      expect(getPotentialSolution(testedCase).length).toBe(6);
      expect(getPotentialSolution(testedCase)).toContain(1);
      expect(getPotentialSolution(testedCase)).toContain(2);
      expect(getPotentialSolution(testedCase)).toContain(3);
      expect(getPotentialSolution(testedCase)).toContain(5);
      expect(getPotentialSolution(testedCase)).toContain(7);
      expect(getPotentialSolution(testedCase)).toContain(8);
    });

      });
    describe('excludeAlignPairOrTripleFromSquareOfCollection()', function() {
      it('should return line without 9 ', function() {

        var testCase0=new SudokuCase([1,2],1);
        var testCase1=new SudokuCase([5,9],10);
        var testCase2=new SudokuCase([5,9],19);
        var testCase3=new SudokuCase([4,9],28);
        var testCase4=new SudokuCase([5,9],37);
        var testCase5=new SudokuCase([6,9],46);
        var testCase6=new SudokuCase([7,9],55);
        var testCase7=new SudokuCase([5,9],64);
        var testCase8=new SudokuCase([8,9],73);
        var collection=[];

        collection.push(testCase0);
        collection.push(testCase1);
        collection.push(testCase2);
        collection.push(testCase3);
        collection.push(testCase4);
        collection.push(testCase5);
        collection.push(testCase6);
        collection.push(testCase7);
        collection.push(testCase8);

        var pairOrTriple={};
        pairOrTriple.cases=[testCase1,testCase2]
        pairOrTriple.solutionToExclude=9;
        pairOrTriple.direction="column";
        var nbSolution=0;
        nbSolution=excludeAlignPairOrTripleFromSquareOfCollection(collection)(pairOrTriple)(nbSolution);

        expect(nbSolution).toBe(6);
        expect(collection[0]).toBe(testCase0);
        expect(collection[0].potentialSolution).toContain(1);
        expect(collection[0].potentialSolution).toContain(2);
        expect(collection[1]).toBe(testCase1);
        expect(collection[1].potentialSolution).toContain(5);
        expect(collection[1].potentialSolution).toContain(9);
        expect(collection[2]).toBe(testCase2);
        expect(collection[2].potentialSolution).toContain(5);
        expect(collection[2].potentialSolution).toContain(9);
        expect(collection[3]).toBe(testCase3);
        expect(collection[3].potentialSolution).toContain(4);
        expect(collection[4]).toBe(testCase4);
        expect(collection[4].potentialSolution).toContain(5);
        expect(collection[5]).toBe(testCase5);
        expect(collection[5].potentialSolution).toContain(6);
        expect(collection[6]).toBe(testCase6);
        expect(collection[6].potentialSolution).toContain(7);
        expect(collection[7]).toBe(testCase7);
        expect(collection[7].potentialSolution).toContain(5);
        expect(collection[8]).toBe(testCase8);
        expect(collection[8].potentialSolution).toContain(8);
      });
  });
});
