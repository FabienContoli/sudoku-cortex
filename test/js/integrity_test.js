import {checkIntegritySudoku,checkIntegrityFromCollection} from "../../src/js/logic/integrity.js";
import wrongSudoku from "../json/failedSudoku/wrong_sudoku.json";
import validSudoku from "../json/easy/sudoku_solution.json";


/*

 grille sudoku

 0 1  2  3  4  5  6  7  8
 9 10 11 12 13 14 15 16 17
18 19 20 21 22 23 24 25 26
27 28 29 30 31 32 33 34 35
36 37 38 39 40 41 42 43 44
45 46 47 48 49 50 51 52 53
54 55 56 57 58 59 60 61 62
63 64 65 66 67 68 69 70 71
72 73 74 75 76 77 78 79 80

*/


describe('integrity', function() {
  describe('checkIntegritySudoku()', function() {
    it('should return false', function() {
      var isValid=checkIntegritySudoku(wrongSudoku);
      expect(isValid).toBe(false);
    });

    it('should return true', function() {
      var isValid=checkIntegritySudoku(validSudoku);
      expect(isValid).toBe(true);
    });
  });

  describe('checkIntegrityFromCollection()', function() {
    it('should return false', function() {
      const wrongCollection=[{"potentialSolution":[8],"index":63},{"potentialSolution":[3],"index":64},{"potentialSolution":[7],"index":65},{"potentialSolution":[1],"index":66},{"potentialSolution":[4],"index":67},{"potentialSolution":[8],"index":68},{"potentialSolution":[5],"index":69},{"potentialSolution":[9],"index":70},{"potentialSolution":[6],"index":7}];
      var isValid=checkIntegrityFromCollection(wrongCollection);
      expect(isValid).toBe(false);
    });

    it('should return true', function() {
      const validCollection=[{"potentialSolution":[7],"index":0},{"potentialSolution":[9],"index":1},{"potentialSolution":[8],"index":2},{"potentialSolution":[4],"index":3},{"potentialSolution":[6],"index":4},{"potentialSolution":[1],"index":5},{"potentialSolution":[2],"index":6},{"potentialSolution":[3],"index":7},{"potentialSolution":[5],"index":8}]
      var isValid=checkIntegrityFromCollection(validCollection);
      expect(isValid).toBe(true);
    });
  });
});
