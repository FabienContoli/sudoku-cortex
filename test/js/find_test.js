import {isInSameRow,isInSameColumn,isInSameSquare} from '../../src/js/logic/find'
import {findRowSudoku,findColumnSudoku,findSquareSudoku,findAlignPairOrTripleInSquare} from '../../src/js/logic/find'
import {createSudokuCaseMap} from './utils/helper'
import {SudokuCase} from '../../src/js/model/sudokuModel'


/*

 grille sudoku

 0 1  2  3  4  5  6  7  8
 9 10 11 12 13 14 15 16 17
18 19 20 21 22 23 24 25 26
27 28 29 30 31 32 33 34 35
36 37 38 39 40 41 42 43 44
45 46 47 48 49 50 51 52 53
54 55 56 57 58 59 60 61 62
63 64 65 66 67 68 69 70 71
72 73 74 75 76 77 78 79 80

*/


describe('find', function() {
  describe('isInSameRow()', function() {
    it('should return true', function() {

      var response=isInSameRow(0)(8);
      expect(response).toBe(true);
    });

    it('should return false', function() {

      var response=isInSameRow(45)(8);
      expect(response).toBe(false);
    });
  });

  describe('isInSameColumn()', function() {
    it('should return true', function() {

      var response=isInSameColumn(0)(9);
      expect(response).toBe(true);

      var response=isInSameColumn(5)(14);
      expect(response).toBe(true);

      var response=isInSameColumn(8)(80);
      expect(response).toBe(true);
    });

    it('should return false', function() {

      var response=isInSameColumn(5)(8);
      expect(response).toBe(false);

      var response=isInSameColumn(15)(32);
      expect(response).toBe(false);
    });
  });

  describe('isInSameSquare()', function() {
    it('should return true', function() {

      var response=isInSameSquare(6)(8);
      expect(response).toBe(true);

      var response=isInSameSquare(3)(14);
      expect(response).toBe(true);

      var response=isInSameSquare(42)(44);
      expect(response).toBe(true);
    });

    it('should return false', function() {

      var response=isInSameSquare(3)(8);
      expect(response).toBe(false);

      var response=isInSameSquare(39)(24);
      expect(response).toBe(false);


      var response=isInSameSquare(44)(36);
      expect(response).toBe(false);
    });
  });



    describe('findRowSudoku()', function() {
      it('should return cases from 9 to 17 ', function() {

        var sudokuCaseMap= createSudokuCaseMap();
        var response=findRowSudoku(sudokuCaseMap)(sudokuCaseMap[12]);
        expect(response.length).toBe(9);
        expect(response[0]).toBe(sudokuCaseMap[9]);
        expect(response[1]).toBe(sudokuCaseMap[10]);
        expect(response[2]).toBe(sudokuCaseMap[11]);
        expect(response[3]).toBe(sudokuCaseMap[12]);
        expect(response[4]).toBe(sudokuCaseMap[13]);
        expect(response[5]).toBe(sudokuCaseMap[14]);
        expect(response[6]).toBe(sudokuCaseMap[15]);
        expect(response[7]).toBe(sudokuCaseMap[16]);
        expect(response[8]).toBe(sudokuCaseMap[17]);
      });
    });

    describe('findColumnSudoku()', function() {
      it('should return cases in column 4 (13 22 31 40 49 58 67 76) ', function() {

        var sudokuCaseMap= createSudokuCaseMap();
        var response=findColumnSudoku(sudokuCaseMap)(sudokuCaseMap[31]);
        expect(response.length).toBe(9);
        expect(response[0]).toBe(sudokuCaseMap[4]);
        expect(response[1]).toBe(sudokuCaseMap[13]);
        expect(response[2]).toBe(sudokuCaseMap[22]);
        expect(response[3]).toBe(sudokuCaseMap[31]);
        expect(response[4]).toBe(sudokuCaseMap[40]);
        expect(response[5]).toBe(sudokuCaseMap[49]);
        expect(response[6]).toBe(sudokuCaseMap[58]);
        expect(response[7]).toBe(sudokuCaseMap[67]);
        expect(response[8]).toBe(sudokuCaseMap[76]);
      });
    });

    describe('findSquareSudoku()', function() {
      it('should return cases from square 33 34 35 42 43 44 51 52 53 ', function() {

        var sudokuCaseMap= createSudokuCaseMap();
        var response=findSquareSudoku(sudokuCaseMap)(sudokuCaseMap[42]);
        expect(response.length).toBe(9);
        expect(response[0]).toBe(sudokuCaseMap[33]);
        expect(response[1]).toBe(sudokuCaseMap[34]);
        expect(response[2]).toBe(sudokuCaseMap[35]);
        expect(response[3]).toBe(sudokuCaseMap[42]);
        expect(response[4]).toBe(sudokuCaseMap[43]);
        expect(response[5]).toBe(sudokuCaseMap[44]);
        expect(response[6]).toBe(sudokuCaseMap[51]);
        expect(response[7]).toBe(sudokuCaseMap[52]);
        expect(response[8]).toBe(sudokuCaseMap[53]);
      });
    });

    describe('findAlignPairOrTripleInSquare()', function() {
      it('should return empty array', function() {

        var testCase0=new SudokuCase([1,2,3,5,7,8],0);
        var testCase1=new SudokuCase([4],1);
        var testCase2=new SudokuCase([9],2);
        var testCase3=new SudokuCase([1,3,5,7],9);
        var testCase4=new SudokuCase([1,3,5,7],10);
        var testCase5=new SudokuCase([6],11);
        var testCase6=new SudokuCase([5,7],18);
        var testCase7=new SudokuCase([1,7],19);
        var testCase8=new SudokuCase([1,3,5,7,8],20);
        var collection=[];

        collection.push(testCase0);
        collection.push(testCase1);
        collection.push(testCase2);
        collection.push(testCase3);
        collection.push(testCase4);
        collection.push(testCase5);
        collection.push(testCase6);
        collection.push(testCase7);
        collection.push(testCase8);
        var pairOrTripleArray=findAlignPairOrTripleInSquare(collection);
        expect(pairOrTripleArray.length).toBe(0);

      });

      it('should return cases from square 4 7 ', function() {

        var testCase0=new SudokuCase([1],0);
        var testCase1=new SudokuCase([2],1);
        var testCase2=new SudokuCase([3],2);
        var testCase3=new SudokuCase([4],9);
        var testCase4=new SudokuCase([5,9],10);
        var testCase5=new SudokuCase([6],11);
        var testCase6=new SudokuCase([7],18);
        var testCase7=new SudokuCase([5,9],19);
        var testCase8=new SudokuCase([8],20);
        var collection=[];

        collection.push(testCase0);
        collection.push(testCase1);
        collection.push(testCase2);
        collection.push(testCase3);
        collection.push(testCase4);
        collection.push(testCase5);
        collection.push(testCase6);
        collection.push(testCase7);
        collection.push(testCase8);

        var pairOrTripleArray=findAlignPairOrTripleInSquare(collection);
        expect(pairOrTripleArray.length).toBe(2);
        var pairOrTriple1=pairOrTripleArray[0];
        expect(pairOrTriple1.cases.length).toBe(2);
        expect(pairOrTriple1.cases[0]).toBe(testCase4);
        expect(pairOrTriple1.cases[1]).toBe(testCase7);
        expect(pairOrTriple1.solutionToExclude).toBe(5);
        expect(pairOrTriple1.direction).toBe("column");

        var pairOrTriple2=pairOrTripleArray[1];
        expect(pairOrTriple2.cases.length).toBe(2);
        expect(pairOrTriple2.cases[0]).toBe(testCase4);
        expect(pairOrTriple2.cases[1]).toBe(testCase7);
        expect(pairOrTriple2.solutionToExclude).toBe(9);
        expect(pairOrTriple2.direction).toBe("column");
      });


      it('should return cases from square 0 9 18 ', function() {

        var testCase0=new SudokuCase([1,2,3,5,7,8],0);
        var testCase1=new SudokuCase([4],1);
        var testCase2=new SudokuCase([9],2);
        var testCase3=new SudokuCase([1,3,5,7,8],9);
        var testCase4=new SudokuCase([1,3,5,7],10);
        var testCase5=new SudokuCase([6],11);
        var testCase6=new SudokuCase([5,7,8],18);
        var testCase7=new SudokuCase([1,7],19);
        var testCase8=new SudokuCase([1,3,5,7],20);
        var collection=[];

        collection.push(testCase0);
        collection.push(testCase1);
        collection.push(testCase2);
        collection.push(testCase3);
        collection.push(testCase4);
        collection.push(testCase5);
        collection.push(testCase6);
        collection.push(testCase7);
        collection.push(testCase8);

        var pairOrTripleArray=findAlignPairOrTripleInSquare(collection);

        expect(pairOrTripleArray.length).toBe(1);
        var pairOrTriple=pairOrTripleArray[0];

        expect(pairOrTriple.cases.length).toBe(3);
        expect(pairOrTriple.cases[0]).toBe(testCase0);
        expect(pairOrTriple.cases[1]).toBe(testCase3);
        expect(pairOrTriple.cases[2]).toBe(testCase6);
        expect(pairOrTriple.solutionToExclude).toBe(8);
        expect(pairOrTriple.direction).toBe("column");

      });
    });
})
