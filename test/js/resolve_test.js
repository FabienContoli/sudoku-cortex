import {resolveSudoku} from "../../src/js/logic/resolve";
import easySudokuSolution from "../json/easy/sudoku_solution.json";
import easySudokuUncomplete from "../json/easy/sudoku_uncomplete.json";
import hardSudokuSolution from "../json/difficult/sudoku_solution.json";
import hardSudokuUncomplete from "../json/difficult/sudoku_uncomplete.json";
import {getPotentialSolution} from '../../src/js/logic/model';

/*

 grille sudoku

 0 1  2  3  4  5  6  7  8
 9 10 11 12 13 14 15 16 17
18 19 20 21 22 23 24 25 26
27 28 29 30 31 32 33 34 35
36 37 38 39 40 41 42 43 44
45 46 47 48 49 50 51 52 53
54 55 56 57 58 59 60 61 62
63 64 65 66 67 68 69 70 71
72 73 74 75 76 77 78 79 80

*/


describe('resolve', function() {
  describe('resolveSudoku()', function() {
    it('should return sudoku easy Resolved', function() {

      var sudokuResolved=resolveSudoku(easySudokuUncomplete);
      expect(sudokuResolved.length).toBe(81);
      for(var i=0;i<sudokuResolved.length;i++){
        expect(getPotentialSolution(sudokuResolved[i]).length).toBe(1);
        expect(getPotentialSolution(sudokuResolved[i])).toContain(getPotentialSolution(easySudokuSolution[i])[0]);
      }
    });

      it('should return sudoku hard Resolved', function() {

        var sudokuResolved=resolveSudoku(hardSudokuUncomplete);
        expect(sudokuResolved.length).toBe(81);
        for(var i=0;i<sudokuResolved.length;i++){
          expect(getPotentialSolution(sudokuResolved[i]).length).toBe(1);
          expect(getPotentialSolution(sudokuResolved[i])).toContain(getPotentialSolution(hardSudokuSolution[i])[0]);
        }
      });

  });
});
