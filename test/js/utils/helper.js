import completeSudoku from '../../json/sudoku-finish.json';
import {SudokuCase} from '../../../src/js/model/sudokuModel';

const createSudokuCaseMap=function(){
  var sudokuCaseMap=[];
  for(let i=0;i<completeSudoku.sudoku.length;i++){
    sudokuCaseMap.push(new SudokuCase(completeSudoku.sudoku[i],i));
  }
  return sudokuCaseMap;
}
export {createSudokuCaseMap};
